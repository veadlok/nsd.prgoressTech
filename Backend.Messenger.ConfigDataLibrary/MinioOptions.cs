﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Messenger.ConfigDataLibrary
{
    public class MinioOptions
    {
        public string Host { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }

    }
}
