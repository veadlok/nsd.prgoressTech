﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace Backend.Messenger.ConfigDataLibrary
{
    public class AuthOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Secret { get; set; }
        public int TokenLifetime { get; set; } // seconds

        public SymmetricSecurityKey SecurityKey
        { get => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Secret)); }
    }
}
