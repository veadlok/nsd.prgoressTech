﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Messenger.ConfigDataLibrary
{
    public class DataAccessOptions
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public string ConnectionString
        { get => $"Host={Host};Port={Port};Database={Database};Username={Username};Password={Password}"; }
    }
}
