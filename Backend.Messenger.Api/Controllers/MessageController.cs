﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend.Messenger.Api.Controllers
{
    [ApiController]
    //[Authorize (Roles = "User")]
    [Route("[controller]")]
    public class MessageController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        public String Get()
        {
            return "Hello world!";
        }
    }
}
