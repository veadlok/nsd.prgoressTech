﻿using Backend.Messenger.ConfigDataLibrary;
using Backend.Messenger.DataAccess.DataAccess;
using Backend.Messenger.DataAccess.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Backend.Messenger.DataAccess.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DataAccessController : Controller
    {
        private readonly MinioContext _minioContext;
        private readonly HashSumService _hashSumService;

        public DataAccessController(IOptions<MinioOptions> minioOptions, HashSumService hashSumService)
        {
            _minioContext = new MinioContext(minioOptions.Value);
            _hashSumService = hashSumService;
        }

        [HttpPost("Upload")]
        [DisableRequestSizeLimit]
        public async Task<FileUploadResponse> Upload(IFormFile file)
        {
            try
            {
                using (var stream = file.OpenReadStream())
                {
                    var objectKey = Guid.NewGuid().ToString();
                    await _minioContext.UploadFile("storage", objectKey, file.FileName, stream, stream.Length);
                    stream.Position = 0;
                    var hashSum = _hashSumService.GetHashSumAsync(stream);
                    return new FileUploadResponse
                    {
                        HashSum = hashSum,
                        ObjectKey = objectKey
                    };
                }
            }
            catch(Exception e)
            {
            }
            return new FileUploadResponse
            {
            };
        }

        [HttpPost("Download")]
        public async Task Download(string objectKey)
        {
            try
            {
                //using (var stream = file.OpenReadStream())
                //{
                //    await _minioContext.UploadFile("storage", "testlocation", file.FileName, stream, stream.Length);
                //}
            }
            catch (Exception e)
            {
            }
        }

        [Route("savefile")]
        [HttpPost]
        [Consumes("multipart/form-data")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> SaveFile([FromBody]IList<IFormFile> chunkFile)
        {
            long size = 0;
            try
            {
                // for chunk-upload
                foreach (var file in chunkFile)
                {
                    var filename = ContentDispositionHeaderValue
                                        .Parse(file.ContentDisposition)
                                        .FileName
                                        .Trim('"');
                    filename = $@"\{filename}";
                    size += file.Length;
                    if (!System.IO.File.Exists(filename))
                    {
                        using (FileStream fs = System.IO.File.Create(filename))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                    else
                    {
                        using (FileStream fs = System.IO.File.Open(filename, FileMode.Append))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //Response.Clear();
                //Response.StatusCode = 204;
                //Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File failed to upload";
                //Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            //using (FileStream fs = new FileStream(@"/app/web.config", FileMode.Open))
            //{
            //    await _minioContext.UploadFile("testbucket", "testlocation", "schemetest.png", fs, fs.Length);
            //}
            return Ok();
        }
    }
}
