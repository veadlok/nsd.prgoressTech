﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Backend.Messenger.DataAccess.Services
{
    public class HashSumService
    {
        public string GetHashSumAsync(Stream stream)
        {
            using (var md5 = MD5.Create())
            {
                var arr = md5.ComputeHash(stream);
                return BitConverter.ToString(arr);
            }
        }
    }
}
