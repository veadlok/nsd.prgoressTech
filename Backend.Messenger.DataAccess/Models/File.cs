﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Messenger.DataAccess.Models
{
    public class File
    {
        public Guid User_Id { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public string Hash { get; set; }
    }
}
