﻿namespace Backend.Messenger.DataAccess.DataAccess
{
    public class FileUploadResponse
    {
        public string ObjectKey { get; set; }
        public string HashSum { get; set; }
    }
}