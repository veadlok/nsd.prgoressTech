﻿using Backend.Messenger.ConfigDataLibrary;
using Minio;
using Minio.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;

namespace Backend.Messenger.DataAccess.DataAccess
{
    public class MinioContext
    {
        private const string CONTENT_TYPE = "application/zip";
        private readonly MinioClient _minioClient;


        public MinioContext(MinioOptions dataAccessOptions)
        {
            _minioClient = new MinioClient(dataAccessOptions.Host, dataAccessOptions.AccessKey, dataAccessOptions.SecretKey);
        }

        public async Task UploadFile(string bucketName, string objectKey, string objectName, Stream fileStream, long fileSize)
        {
            await CreateIfBucketNotExists(bucketName);
            await _minioClient.PutObjectAsync(bucketName, objectName, fileStream, fileSize, contentType: CONTENT_TYPE);
        }

        public async Task CreateIfBucketNotExists(string bucketName)
        {
            bool found = await _minioClient.BucketExistsAsync(bucketName);
            if (!found)
            {
                await _minioClient.MakeBucketAsync(bucketName);
            }
        }
        
        public async Task<SelectResponseStream> DownloadFile(string objectKey, string bucketName)
        {
            return await _minioClient.SelectObjectContentAsync(bucketName, objectKey, new SelectObjectOptions());
        }
    }
}
