using Backend.Messenger.DataAccess.Models;
using Backend.Messenger.ConfigDataLibrary;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Messenger.DataAccess.DataAccess
{
    public class PostreSqlDbContext : DbContext
    {
        #region Public members
        public DbSet<File> Files { get; set; }
        #endregion Public members

        #region Private members
        private DataAccessOptions _dataAccessOptions;
        #endregion Private members

        #region Ctors
        public PostreSqlDbContext(DataAccessOptions dataAccessOptions)
        {
            _dataAccessOptions = dataAccessOptions;
            Database.EnsureCreated();
        }
        #endregion Ctors

        #region Public methods
        public IEnumerable<File> GetUserFiles(Guid user_id)
            => Files.Where((file) => user_id == file.User_Id).ToList();

        public bool AddFileData(Guid user_id, string name, string size, string hash)
        {
            Files.Add(new File() { User_Id = user_id, Name = name, Size = size, Hash = hash});
            return SaveChanges() > 0;
        }
        #endregion Public methods

        #region Protected override methods
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_dataAccessOptions.ConnectionString);
        }
        #endregion Protected override methods
    }
}