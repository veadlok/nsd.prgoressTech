﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Messenger.TelegramBot.Data
{
    public class BotSettings
    {
        public string Token { get; set; }
        public string NgrokHost { get; set; }
    }
}
