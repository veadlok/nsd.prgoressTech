﻿namespace Backend.Messenger.TelegramBot.Data
{
    public class ApplicationInfoSettings
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}
