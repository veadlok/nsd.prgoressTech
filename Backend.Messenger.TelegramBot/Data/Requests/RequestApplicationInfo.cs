﻿using Backend.Messenger.TelegramBot.Data;
using MediatR;

namespace Backend.Messenger.TelegramBot.Services.Requests
{
    /// <summary>
    ///     Naming Convention: Обязательный префикс Request
    /// </summary>
    public class RequestApplicationInfo : IRequest<ApplicationInfoSettings>
    {
        public string RequestParametrExample { get; set; } = "Example request parametr";
    }
}
