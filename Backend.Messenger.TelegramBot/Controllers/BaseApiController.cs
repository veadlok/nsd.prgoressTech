﻿using Microsoft.AspNetCore.Mvc;

namespace Backend.Messenger.TelegramBot.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public abstract class BaseApiController : Controller
    {
    }
}
