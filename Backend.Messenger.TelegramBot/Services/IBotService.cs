﻿using Backend.Messenger.TelegramBot.Data;
using Backend.Messenger.TelegramBot.Data.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Messenger.TelegramBot.Services
{
    /// <summary>
    ///     Бот-обработчик
    /// </summary>
    public interface IBotService
    {
        /// <summary>
        ///     Обработать новое сообщения
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns>Результат обработки сообщения</returns>
        Task<OperationResult<BotResult>> HandleNewMessageAsync(BotMessage message, CancellationToken token = default);
    }
}
