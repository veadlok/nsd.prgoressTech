﻿using Backend.Messenger.TelegramBot.Data;

namespace Backend.Messenger.TelegramBot.Services
{
    public interface IApplicationInfoService
    {
        ApplicationInfoSettings GetApplicationInfo();
    }
}