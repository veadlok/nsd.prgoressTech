﻿using Backend.Messenger.TelegramBot.Data;
using Backend.Messenger.TelegramBot.Data.Requests;
using Backend.Messenger.TelegramBot.Data.Results;
using Backend.Messenger.TelegramBot.Services.Requests;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Messenger.TelegramBot.Services.Handlers
{
    public class NewMessageHandler : IRequestHandler<RequestNewMessage, OperationResult<BotResult>>
    {
        private readonly IBotService _bot;
        public NewMessageHandler(IBotService bot)
        {
            _bot = bot;
        }

        public Task<OperationResult<BotResult>> Handle(RequestNewMessage request, CancellationToken cancellationToken)
            => _bot.HandleNewMessageAsync(request.Message, cancellationToken);
    }
}
