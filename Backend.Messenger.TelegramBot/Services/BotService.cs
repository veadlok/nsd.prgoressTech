﻿using Backend.Messenger.TelegramBot.Data;
using Backend.Messenger.TelegramBot.Data.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Messenger.TelegramBot.Services
{
    public class BotService : IBotService
    {
        public BotService()
        {
        }

        public async Task<OperationResult<BotResult>> HandleNewMessageAsync(BotMessage message, CancellationToken token)
        {
            if (message.Text == "/statistic")
            {
                var successResult = new OperationResult<BotResult>(new BotResult
                {
                    Text = "Текущая статика:"
                    + "\nПолучено файлов - 14"
                    + "\nОтдано файлов - 3"
                    + "\nЗанятое место - 34.5 Гб"
                });
                return successResult;
            }
            else
            {
                var errorResult = new OperationResult<BotResult>(new BotResult
                {
                    Text = "Неизвестная команда"
                });
                return errorResult;
            }
        }
    }
}
