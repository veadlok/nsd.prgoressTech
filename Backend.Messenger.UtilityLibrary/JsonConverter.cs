﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Messenger.UtilityLibrary
{
    public class JsonConverter
    {
        public JsonSerializerSettings CamelSettings { get; private set; }

        public JsonConverter()
        {
            CamelSettings = new JsonSerializerSettings()
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                }
            };
        }

        public string SerializeObject<T>(T contract)
            => JsonConvert.SerializeObject(contract, Formatting.None, CamelSettings);

        public T DeserializeObject<T>(string json)
            => JsonConvert.DeserializeObject<T>(json);
    }
}
