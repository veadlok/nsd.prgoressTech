﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Pathoschild.Http.Client;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Messenger.UtilityLibrary
{
    public class HttpRequestSender
    {
        private readonly IClient _client;
        private readonly JsonConverter _jsonConverter;

        public HttpRequestSender()
        {
            _client = new FluentClient();
            _jsonConverter = new JsonConverter();
        }

        public async Task<T> SendGetAsync<T>(string requestUri)
         => await _client.GetAsync(requestUri).As<T>();

        public async Task<T> SendPostAsync<T>(string requestUri, object content)
            => await _client.PostAsync(requestUri).WithBody(GetContent(content)).As<T>();

        private StringContent GetContent<T>(T contract)
            => new StringContent(_jsonConverter.SerializeObject(contract), Encoding.UTF8, "application/json");
    }
}
