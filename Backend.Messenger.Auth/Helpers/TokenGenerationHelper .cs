﻿using Backend.Messenger.ConfigDataLibrary;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend.Messenger.Auth.Helpers
{
    public class TokenGenerationHelper
    {
        public string GenerateJWT(IOptions<AuthOptions> authOptions, Dictionary<string, List<string>> claimsDict,  string securityAlgorithm = SecurityAlgorithms.HmacSha256)
        {
            var authParams = authOptions.Value;
            var securityKey = authParams.SecurityKey;
            var credentials = new SigningCredentials(securityKey, securityAlgorithm);

            var claims = new List<Claim>();

            foreach(var typeValues in claimsDict)
            {
                foreach (var value in typeValues.Value)
                {
                    claims.Add(new Claim(typeValues.Key, value));
                }
            }

            var token = new JwtSecurityToken(authParams.Issuer, authParams.Audience, claims,
                expires: DateTime.Now.AddSeconds(authParams.TokenLifetime), signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
