﻿using Backend.Messenger.Auth.DataAccess;
using Backend.Messenger.Auth.Helpers;
using Backend.Messenger.Auth.Models;
using Backend.Messenger.ConfigDataLibrary;
using Backend.Messenger.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace NSD.Progress.Tech.Backend.Messenger.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        #region Private members
        private readonly IOptions<AuthOptions> _authOptions;
        private readonly TokenGenerationHelper _tokenGenerationHelper;
        private readonly PostreSqlDbContext _postreSqlDbContext;
        #endregion Private members

        #region Ctors
        public AuthController(IOptions<AuthOptions> authOptions, IOptions<DataAccessOptions> dataAccessOptions)
        {
            _authOptions = authOptions;
            _tokenGenerationHelper = new TokenGenerationHelper();
            _postreSqlDbContext = new PostreSqlDbContext(dataAccessOptions.Value);
        }
        #endregion Ctors

        #region Public methods
        [Route("login")]
        [HttpPost]
        public IActionResult Login([FromBody]Login request)
        {
            IActionResult result = Unauthorized();

            var user = _postreSqlDbContext.GetUserData(request.Email, request.Password)?.FirstOrDefault();

            if (user != null)
            {
                var claimsDict = new Dictionary<string, List<string>>()
                {
                    { JwtRegisteredClaimNames.Email, new List<string>(){ user.Email } },
                    { JwtRegisteredClaimNames.Sub, new List<string>(){ user.Id.ToString() } },
                };

                if (user.Roles != null)
                {
                    var roleKey = "role";

                    foreach (var role in user.Roles)
                    {
                        if (claimsDict.ContainsKey(roleKey))
                        {
                            claimsDict[roleKey].Add(role);
                        }
                        else
                        {
                            claimsDict.Add(roleKey, new List<string>() { role });
                        }
                    }
                }

                result = Ok(new { access_token = _tokenGenerationHelper.GenerateJWT(_authOptions, claimsDict) });
            }
            return result;
        }

        [Route("register")]
        [HttpPost]
        public IActionResult Register([FromBody] Login request)
            => _postreSqlDbContext.GetUserData(request.Email)?.FirstOrDefault() == null
                ? _postreSqlDbContext.AddUserData(request.Email, request.Password)
                    ? Ok()
                    : UnprocessableEntity()
                : Conflict();
        #endregion Public methods
    }
}
