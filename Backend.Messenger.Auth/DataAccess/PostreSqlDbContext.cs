﻿using Backend.Messenger.Auth.Models;
using Backend.Messenger.ConfigDataLibrary;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Messenger.Auth.DataAccess
{
    public class PostreSqlDbContext : DbContext
    {
        #region Public members
        public DbSet<Account> Accounts { get; set; }
        #endregion Public members

        #region Private members
        private DataAccessOptions _dataAccessOptions;
        #endregion Private members

        #region Ctors
        public PostreSqlDbContext(DataAccessOptions dataAccessOptions)
        {
            _dataAccessOptions = dataAccessOptions;
            Database.EnsureCreated();
        }
        #endregion Ctors

        #region Public methods
        public IEnumerable<Account> GetUserData(string email = null, string password = null)
            => email != null || password != null
                ? Accounts.Where((acc) => (email ?? acc.Email) == acc.Email && (password ?? acc.Password) == acc.Password).ToList()
                : null;

        public bool AddUserData(string email, string password)
        {
            Accounts.Add(new Account() { Email = email, Password = password });
            return SaveChanges() > 0;
        }
        #endregion Public methods

        #region Protected override methods
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_dataAccessOptions.ConnectionString);
        }
        #endregion Protected override methods
    }
}
